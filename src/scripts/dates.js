const friendlyDate = (date, now) => {
  const deltaMilliseconds = now - date
  const deltaSeconds = Math.floor(deltaMilliseconds / 1000)
  const deltaMinutes = Math.floor(deltaSeconds / 60)
  const deltaHours = Math.floor(deltaMinutes / 60)
  const deltaDays = Math.floor(deltaHours / 24)
  const deltaMonths = Math.floor(deltaDays / 30)

  if (deltaSeconds < 5) {
    return 'un attimo fa'
  } else if (deltaSeconds < 60) {
    return deltaSeconds + ' secondi fa'
  } else if (deltaMinutes === 1) {
    return '1 minuto fa'
  } else if (deltaMinutes < 60) {
    return deltaMinutes + ' minuti fa'
  } else if (deltaHours === 1) {
    return '1 ora fa'
  } else if (deltaHours < 24) {
    return deltaHours + ' ore fa'
  } else if (deltaDays === 1) {
    return '1 giorno fa'
  } else if (deltaDays < 30) {
    return deltaDays + ' giorni fa'
  } else if (deltaMonths === 1) {
    return '1 mese fa'
  } else {
    return deltaMonths + ' mesi fa'
  }
}

export { friendlyDate }
