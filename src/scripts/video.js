const importFileandPreview = (file, revoke) => {
  return new Promise((resolve) => {
    window.URL = window.URL || window.webkitURL
    let preview = window.URL.createObjectURL(file)
    // remove reference
    if (revoke) {
      window.URL.revokeObjectURL(preview)
    }
    setTimeout(() => {
      resolve(preview)
    }, 100)
  })
}

const getVideoThumbnail = (file) => {
  return new Promise((resolve, reject) => {
    if (file?.type?.match('video')) {
      importFileandPreview(file).then((urlOfFIle) => {
        generateVideoThumbnailViaUrl(urlOfFIle).then((res) => {
          resolve(res)
        })
      })
    } else {
      reject('file not valid')
    }
  })
}

const generateVideoThumbnailViaUrl = (urlOfFIle) => {
  return new Promise((resolve, reject) => {
    try {
      var video = document.createElement('video')
      var timeupdate = function () {
        if (snapImage()) {
          video.removeEventListener('timeupdate', timeupdate)
          video.pause()
        }
      }
      video.addEventListener('loadeddata', function () {
        if (snapImage()) {
          video.removeEventListener('timeupdate', timeupdate)
        }
      })
      var snapImage = function () {
        var canvas = document.createElement('canvas')
        canvas.width = video.videoWidth
        canvas.height = video.videoHeight
        canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height)
        var image = canvas.toDataURL()
        var success = image.length > 100000
        if (success) {
          URL.revokeObjectURL(urlOfFIle)
          resolve(image)
        }
        return success
      }
      video.addEventListener('timeupdate', timeupdate)
      video.preload = 'metadata'
      video.src = urlOfFIle
      // Load video in Safari / IE11
      video.muted = true
      video.playsInline = true
      video.setAttribute('crossOrigin', '')
      video.currentTime = 1
      video
        .play()
        .then()
        .catch((err) => {
          reject({
            status: 500,
            reason: `Access to video at ${urlOfFIle} from origin ${window.location.hostname} has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource.`,
            message: err
          })
        })
    } catch (error) {
      reject(error)
    }
  })
}

export const generateVideoThumbnail = (videoFile) => {
  return new Promise((resolve, reject) => {
    if (!videoFile.type?.includes('video')) {
      reject('not a valid video file')
    }

    getVideoThumbnail(videoFile)
      .then((thumbnail) => {
        resolve(thumbnail)
      })
      .catch((err) => {
        console.error(err)
        reject(err)
      })
  })
}
