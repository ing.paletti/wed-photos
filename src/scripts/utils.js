const orderNumerically = (a, b, num) => {
  const num1 = a[num]
  const num2 = b[num]

  return num1 > num2 ? 1 : num2 > num1 ? -1 : 0
}

export { orderNumerically }
