import cookie from 'js-cookie'

const setCookie = (name, value, longExpire) => {
  const expire = (longExpire ? 365 * 24 * 60 : 8 * 60) * 60 * 1000
  const date = new Date()
  date.setTime(date.getTime() + expire)
  cookie.set(name, value, {
    path: '/',
    domain: location.hostname,
    expires: date,
    secure: location.protocol === 'https',
    sameSite: 'strict'
  })
}

const getCookie = cookie.get
const removeCookie = cookie.remove

export { setCookie, getCookie, removeCookie }
